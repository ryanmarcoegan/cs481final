﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
//adding analytics
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
//UWPapp secret 63fa5fff-fffc-4374-9079-0ab110fe2781


[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace mobilefinal
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
            
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            AppCenter.Start("android=63fa5fff-fffc-4374-9079-0ab110fe2781;" + "uwp={63fa5fff-fffc-4374-9079-0ab110fe2781};" + "ios={Your iOS App secret here}", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
      
    }
}
