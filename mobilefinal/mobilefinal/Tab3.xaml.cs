﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AppCenter.Analytics;


namespace mobilefinal
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Tab3 : ContentPage
	{
        //var ticketdata;
       // public List<Ticket1> ticket2;
        public Tab3 ()
		{
			InitializeComponent ();
		}
        

        async void Button_Clicked(object sender, EventArgs e)
        {
            Spinner.IsRunning = true;
            Analytics.TrackEvent("Ticket Search clicked");
            var Client = new HttpClient();


            var TicketApiAddress = "http://app.ticketmaster.com/discovery/v1/events.json?keyword=" + SearchTicket.Text + "&apikey=NBVSbV7vQF0x3TGplbwFUSdtk0DK2mnl&callback=myFunction";


            var uri = new Uri(TicketApiAddress);
            var ticketdata = new RootObject();


            var response = await Client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
               ticketdata = JsonConvert.DeserializeObject<RootObject>(jsonContent);
            }


            if (ticketdata._embedded == null)
            {
                await DisplayAlert("Invalid Event", "Event not found", "Try again");
            }
            else
              
            TicketListView.ItemsSource = ticketdata._embedded.events;


            Spinner.IsRunning = false;
          
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {

        }
    }
}


