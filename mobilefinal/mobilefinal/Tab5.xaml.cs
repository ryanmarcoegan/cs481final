﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json;

namespace mobilefinal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tab5 : ContentPage
    {
        
        public Tab5()
        {
            InitializeComponent();
            GetGenre();
        }

        async void GetGenre()
        {

            var Client = new HttpClient();

            var GenreApiAddress = "http://ws.audioscrobbler.com/2.0/?method=chart.gettoptags&api_key=e4eac43eb156d5cc84283214b711c99b&format=json";
            var uri = new Uri(GenreApiAddress);

            var genredata = new RootObject1();
           

            var response = await Client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                genredata = JsonConvert.DeserializeObject<RootObject1>(jsonContent);
            }

           


            GenreListView.ItemsSource = genredata.tags.tag;
     

        }
      
    }
}