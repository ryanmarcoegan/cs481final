﻿namespace mobilefinal
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Song1
    {
        [JsonProperty("results")]
        public Results1 Results1 { get; set; }
    }

    public partial class Results1
    {
        [JsonProperty("opensearch:Query")]
        public OpensearchQuery OpensearchQuery { get; set; }

        [JsonProperty("opensearch:totalResults")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long OpensearchTotalResults { get; set; }

        [JsonProperty("opensearch:startIndex")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long OpensearchStartIndex { get; set; }

        [JsonProperty("opensearch:itemsPerPage")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long OpensearchItemsPerPage { get; set; }

        [JsonProperty("trackmatches")]
        public Trackmatches Trackmatches { get; set; }

        [JsonProperty("@attr")]
        public Attr Attr { get; set; }
    }

    public partial class Attr
    {
    }

   

    public partial class Trackmatches
    {
        [JsonProperty("track")]
        public Track[] Track { get; set; }
    }

    public partial class Track
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("artist")]
        public string Artist { get; set; }

        [JsonProperty("url")]
        public Uri Url { get; set; }

        [JsonProperty("streamable")]
        public Streamable Streamable { get; set; }

        [JsonProperty("listeners")]
        [JsonConverter(typeof(ParseStringConverter))]
        public long Listeners { get; set; }

        [JsonProperty("image")]
        public Image[] Image { get; set; }

        [JsonProperty("mbid")]
        public string Mbid { get; set; }
    }


    public enum Size { Extralarge, Large, Medium, Small };

    public enum Streamable { Fixme };

 
       
    internal class SizeConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Size) || t == typeof(Size?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            switch (value)
            {
                case "extralarge":
                    return Size.Extralarge;
                case "large":
                    return Size.Large;
                case "medium":
                    return Size.Medium;
                case "small":
                    return Size.Small;
            }
            throw new Exception("Cannot unmarshal type Size");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Size)untypedValue;
            switch (value)
            {
                case Size.Extralarge:
                    serializer.Serialize(writer, "extralarge");
                    return;
                case Size.Large:
                    serializer.Serialize(writer, "large");
                    return;
                case Size.Medium:
                    serializer.Serialize(writer, "medium");
                    return;
                case Size.Small:
                    serializer.Serialize(writer, "small");
                    return;
            }
            throw new Exception("Cannot marshal type Size");
        }

        public static readonly SizeConverter Singleton = new SizeConverter();
    }

    internal class StreamableConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(Streamable) || t == typeof(Streamable?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            if (value == "FIXME")
            {
                return Streamable.Fixme;
            }
            throw new Exception("Cannot unmarshal type Streamable");
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (Streamable)untypedValue;
            if (value == Streamable.Fixme)
            {
                serializer.Serialize(writer, "FIXME");
                return;
            }
            throw new Exception("Cannot marshal type Streamable");
        }

        public static readonly StreamableConverter Singleton = new StreamableConverter();
    }
}
