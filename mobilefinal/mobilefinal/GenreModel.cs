﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace mobilefinal
{
    public class Wiki
    {
    }

    public class Tag
    {
        public string name { get; set; }
        public string url { get; set; }
        public string reach { get; set; }
        [JsonProperty("taggings")]
        public string Count { get; set; }
        public string streamable { get; set; }
        public Wiki wiki { get; set; }
    }

    public class Attr1
    {
        public string page { get; set; }
        public string perPage { get; set; }
        public string totalPages { get; set; }
        public string total { get; set; }
    }

    public class Tags
    {
        public List<Tag> tag { get; set; }
        //public Attr1 __invalid_name__@attr { get; set; }

    }
    public class RootObject1
    {
        public Tags tags { get; set; }
    }
}


