﻿namespace mobilefinal
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;


    public class Self
    {
        public string href { get; set; }
        public bool templated { get; set; }
    }

    public class Links
    {
        public Self self { get; set; }
    }

    public class Start
    {
        public DateTime dateTime { get; set; }
        public string localDate { get; set; }
        public string localTime { get; set; }
    }

    public class Status
    {
        public string code { get; set; }
    }

    public class End
    {
        public DateTime dateTime { get; set; }
        public string localDate { get; set; }
        public string localTime { get; set; }
    }

    public class Range
    {
        public string localStartDate { get; set; }
        public string localEndDate { get; set; }
    }

    public class DisplayOptions
    {
        public Range range { get; set; }
    }

    public class Dates
    {
        public Start start { get; set; }
        public string timezone { get; set; }
        public Status status { get; set; }
        public End end { get; set; }
        public DisplayOptions displayOptions { get; set; }
    }

    public class Public
    {
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
    }

    public class Sales
    {
        public Public @public { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
    }

    public class Venue
    {
        public string href { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
        public object categories { get; set; }
        public object attractions { get; set; }
        public Venue venue { get; set; }
    }

    public class Country
    {
        public string countryCode { get; set; }
    }

    public class State
    {
        public string stateCode { get; set; }
    }

    public class City
    {
        public string name { get; set; }
    }

    public class Location
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

    public class Address
    {
        public string line1 { get; set; }
        public string line2 { get; set; }
    }

    public class Self3
    {
        public string href { get; set; }
    }

    public class Links3
    {
        public Self3 self { get; set; }
    }

    public class Venue2
    {
        public string name { get; set; }
        public List<int> marketId { get; set; }
        public Country country { get; set; }
        public State state { get; set; }
        public City city { get; set; }
        public Location location { get; set; }
        public string postalCode { get; set; }
        public Address address { get; set; }
        public string timeZone { get; set; }
        public string type { get; set; }
        public Links3 _links { get; set; }
        public string id { get; set; }
    }

    public class Self4
    {
        public string href { get; set; }
    }

    public class Links4
    {
        public Self4 self { get; set; }
    }

    public class Category
    {
        public string name { get; set; }
        public int level { get; set; }
        public Links4 _links { get; set; }
        public string id { get; set; }
        public string type { get; set; }
    }
    /*
    public class Image
    {
        public string url { get; set; }
    }
    */

    public class Self5
    {
        public string href { get; set; }
    }

    public class Links5
    {
        public Self5 self { get; set; }
    }

    public class Attraction
    {
        public string url { get; set; }
        public Image image { get; set; }
        public string name { get; set; }
        public Links5 _links { get; set; }
        public string id { get; set; }
        public string type { get; set; }
    }

    public class Embedded2
    {
        public List<Venue2> venue { get; set; }
        public List<Category> categories { get; set; }
        public List<Attraction> attractions { get; set; }
    }

    public class Event
    {
        public string name { get; set; }
        public string locale { get; set; }
        public string eventUrl { get; set; }
        public Dates dates { get; set; }
        public bool test { get; set; }
        public Sales sales { get; set; }
        public Links2 _links { get; set; }
        public string id { get; set; }
        public Embedded2 _embedded { get; set; }
        public string type { get; set; }
        public List<int?> promoterId { get; set; }
    }

    public class Embedded
    {
        public List<Event> events { get; set; }
    }

    public class Page
    {
        public int size { get; set; }
        public int totalElements { get; set; }
        public int totalPages { get; set; }
        public int number { get; set; }
    }

    public class RootObject
    {
        public Links _links { get; set; }
        public Embedded _embedded { get; set; }
        public Page page { get; set; }
    }
}



