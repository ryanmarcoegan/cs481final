﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AppCenter.Analytics;


namespace mobilefinal
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tab1 : ContentPage
    {
     
        public Tab1()
        {
            InitializeComponent();
           
        }

        private void SearchArtist_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        async void Button_Clicked(object sender, EventArgs e)
        {

            Spinner.IsRunning = true;
            Analytics.TrackEvent("Artist Search Clicked");
            var Client = new HttpClient();
            
            
       
            var ArtistApiAddress = "http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=" + SearchArtist.Text + "&limit=1&api_key=e4eac43eb156d5cc84283214b711c99b&format=json";
         // var ArtistApiAddress = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=" + SearchArtist.Text + "&limit=1&api_key=e4eac43eb156d5cc84283214b711c99b&format=json";
            var uri = new Uri(ArtistApiAddress);

             var artistdata = new Test();
            //var artistI = new Test();
            var artistI = new Test();

            var response = await Client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                artistdata = JsonConvert.DeserializeObject<Test>(jsonContent);
                artistI = JsonConvert.DeserializeObject<Test>(jsonContent);
            }
            /*
           if(artistdata.Results.Artistmatches.Artist[0].Mbid.ToString() == "")

            {
                await DisplayAlert("Invalid Artist", "Artist not found", "Try again");
            }
            
            if (artistdata.Results.Artistmatches.Artist[0].Mbid == null)
            {
                await DisplayAlert("Invalid Artist", "Artist not found", "Try again");
            }
            */
            if (artistdata.Results.OpensearchTotalResults == 0)
            {
                await DisplayAlert("Invalid Artist", "Artist not found", "Try again");
            }

            else if (artistdata.Results.OpensearchTotalResults != 0)
            {



                ArtistListView.ItemsSource = artistdata.Results.Artistmatches.Artist;

                var pic = artistI.Results.Artistmatches.Artist[0].Image[4];
                ArtistImage.Source = pic.Text;
                await ArtistImage.FadeTo(1, 1500);

                //ArtistListView.ItemsSource= artistI.Results.Artistmatches.Artist[0].Image[4];
                Spinner.IsRunning = false;

            }
           

            /*
            if (artistdata.Results.Artistmatches.Artist[0].Image != null)
            {
                ArtistImage.Text = artistdata.Results.Artistmatches.Artist[0].Image;
            }
            */

        }

        protected void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://www.last.fm/music/" + SearchArtist.Text));
        }
    }
 
    

    }
