﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;
using Microsoft.AppCenter.Analytics;
//testing if marco can push
namespace mobilefinal
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();


        }

        void OnArtistsTapped(object sender, System.EventArgs e)
        {
            Analytics.TrackEvent("Artist Tab Clicked");
            Navigation.PushAsync(new Tab1());
        }

        void OnTicketsTapped(object sender, System.EventArgs e)
        {
            Analytics.TrackEvent("Tickets Tab Clicked");
            Navigation.PushAsync(new Tab3());
        }

        void OnSongsTapped(object sender, System.EventArgs e)
        {
            Analytics.TrackEvent("Songs Tab Clicked");
            Navigation.PushAsync(new Tab4());
        }

        void OnTagsTapped(object sender, System.EventArgs e)
        {
            Analytics.TrackEvent("Top Tags Tab Clicked");
            Navigation.PushAsync(new Tab5());
        }
    }
}

