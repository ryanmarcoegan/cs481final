﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.AppCenter.Analytics;

namespace mobilefinal
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Tab4 : ContentPage
	{
		public Tab4 ()
		{
			InitializeComponent ();
		}

     

        async void Button_Clicked_1(object sender, EventArgs e)
        {
            Spinner.IsRunning = true;
            Analytics.TrackEvent("Song Search Clicked");
            var Client = new HttpClient();


            var SongApiAddress = "http://ws.audioscrobbler.com/2.0/?method=track.search&track=" + SearchSong.Text + "&api_key=e4eac43eb156d5cc84283214b711c99b&format=json";


            var uri = new Uri(SongApiAddress);
            var songdata = new Song1();


            var response = await Client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                songdata = JsonConvert.DeserializeObject<Song1>(jsonContent);
            }


          if(songdata.Results1.OpensearchTotalResults == 0)
            {
                await DisplayAlert("Invalid Song", "Song not found", "Try again");
            }
          else


            SongListView.ItemsSource = songdata.Results1.Trackmatches.Track;
            Spinner.IsRunning = false;
        }
    }
}